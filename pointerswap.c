#include<stdio.h>

int main ()
{
  int a, b;
  printf ("Enter the first number: ");
  scanf ("%d", &a);
  printf ("Enter the second number: ");
  scanf ("%d", &b);
  swap (&a, &b);
  printf ("Ater swapping\n First number = %d\n Second number = %d \n", a, b);
  return 0;
}

void swap (int *a, int *b)
{
  int temp;
  temp = *a;
  *a = *b;
  *b = temp;
}