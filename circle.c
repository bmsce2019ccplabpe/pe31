#include<stdio.h>
#include<math.h>
float input_radius()
{
    float r;
    printf("Enter the radius\n"); 
    scanf("%f",&r);
    return r;
}
float compute_area(float r)
{
    M_PI*r*r;
    return M_PI*r*r;
}
void output_area(float r,float area)
{
    printf("The area of circle with radius=%f is %f\n",r,area);
}
float compute_circumference(float r)
{
    2*M_PI*r;
    return 2*M_PI*r;
}
void output_circumference(float r,float circumference)
{
    printf("The circumference of a circle with radius=%f is %f\n",r,circumference);
}
int main()
{
    float r,area,circumference;
    r=input_radius();
    area=compute_area(r);
    circumference=compute_circumference(r);
    output_area(r,area);
    output_circumference(r,circumference);
    return 0;
}