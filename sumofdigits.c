#include <stdio.h>

int findsum(int num)
{	int x, sum;
	sum = 0;
	while (num!=0)
		{	x = num%10;
		    sum = sum + x;
		    num = num/10; }
	return sum; }

int main ()
{
	int num, res;
	scanf("%d" , &num);
	res = findsum(num);
	printf("%d" ,res);
	return 0;
}

